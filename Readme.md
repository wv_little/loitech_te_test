
# 概要

これは、Loitechのウェブサイトのソースコードです。
Middlemanという、Rubyのフレームワークを使用しています。
コンパイルすると静的なHTML、CSS、JS等を出力します。
入力にはSlimという書式を使用しています。
CSSはSCSSという書式を使用していますが、CSSファイルをSCSSファイルとして読み込むことができますので、どうしてもわからなければCSSでも構いません。

#ファイル構造

> source/	ソースが入っている

> > layouts/	ページ全体の構成が入っている
> > > layout.slim	デフォルトのページの構成

> > partials/	ページを構成するパーツが入っている
> > > \_headers.slim	ヘッダの目次(data/header\_index.ymlを読み込んでいる)
> > > \_sidebar.slim	サイドバー
> > > \_footer.slim	フッター

> > images/	画像が入っている

> > javascripts/	jsファイルが入っている

> > stylesheets/	スタイルシートが入っている
> > > style.css.scss	SCSSファイル。コンパイルするとstyle.cssになる。

> > index.html.slim	コンパイルするとindex.htmlになる

> data/	データが入っている。
> > header\_index.yml

# 補足事項

slimファイル上部に

```
----
title: トップページ | Loitech
---
```

のように書かれているが、これでタイトルを設定できる。
何も指定しないと、layouts/layout.slimで指定された
"Loitech 公式サイト"
になる。

# コマンド等

* ローカルサーバの実行
  * `./preview`
    * 中身は `bundle exec middleman server`
  * ポート番号 `4567` でアクセス可能
  * 変更は、保存した時点でリアルタイムに反映される

* ビルド (コンパイル)
  * `./deploy`
    * 中身は `bundle exec middleman build`
  * `build/` に一通りのファイルが作られる 

# 新規環境 (Ubuntu 14.04 LTS) でビルドするまで

Root権限で走らせる場合
```
git clone ***
sudo apt update -y && sudo apt upgrade -y
sudo apt install make gcc g++ ruby-bundler  ruby-dev
sudo gem install bundler 
sudo bundle install
bundle exec middleman build
```


